const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/carsdb', {useNewUrlParser: true, useUnifiedTopology: true})
  .then(() => console.log('Conectado correctamente a MongoDB')) 
  .catch(() => console.log('Error en la conexión a MongoDB')) 

const carSchema = new mongoose.Schema({
  company: String,
  model: String,
  price: Number,
  year: Number,
  sold: Boolean,
  extras: [String],
  update: {type: Date, default: Date.now}
})

// CLASE 
const Car = mongoose.model('carCollection', carSchema)

// createCar();

// getAllCars();
// getCompanyAndSoldFilterCars();
// getMoreFilterCar();
// getFilterPriceCars();
// getFilterPriceInNinCars();
// getFilterPriceAndOrCars();
// getCountCar()
// getPaginationCars()

// updateCar('5ec35b98895a0129985aa74c');
// updateFirstCar('5ec35e2560b26c266c61ae2b')

deleteCar('5ec488d7f34e3537c4dc0de5')


/**
* DELETE
*/
async function deleteCar(id) {
  const result = await Car.deleteOne({_id: id})
  console.log(result)
}


/**
* UPDATE
*/
async function updateFirstCar(id) {
  // const result = await Car.update(
  const result = await Car.updateOne(
    {_id: id},
    {
      $set:{
        company: 'Audi',
        model: 'A1'
      }
    }
  )
  console.log(result)
}

async function updateCar(id) {
  const car = await Car.findById(id)
  if(!car) return
  
  car.company = 'Mercedes'
  car.model = 'Clase A'

  const result = await car.save()
  console.log(result)
}


/**
* GET
*/
async function getPaginationCars() {
  // const pageNumber = 2
  const pageNumber = 1
  // const pageSize = 2
  const pageSize = 4

  const cars = await Car 
    .find()
    .skip((pageNumber-1)*pageSize) // Devuelve todos los resultados de la busqueda pero salta los anteriores al indice
    .limit(pageSize)

    console.log(cars)
}

async function getCountCar() {
  const cars = await Car
    // .find()
    .find({company: 'Audi'})
    .count()

  console.log(cars)
}


/**
 * OPERADORES GET. Opercaión lógicas en un array de expresiones
 * "$and" => "Y" selecciona los documentos que cumplen TODAS las expresiones del array
 * "$or" => "O" selecciona los documentos que cumplen AL MENOS UNA de las expresiones del array
 */
async function getFilterPriceAndOrCars() {
  const cars = await Car
    .find()
    // .and([{company: "BMW"}, {model: "X3"}])
    .or([{company: "Audi"}, {model: "X3"}])

  console.log(cars)
}


/**
 * OPERADORES GET 
 * "$in" => Coinicidencias con los valores de un array especificado
 * "$nin" => NO coinicidencias con los valores de un array especificado
 */
async function getFilterPriceInNinCars() {
  const cars = await Car
    // .find({extras: {$ni: 'Automatic', $nin: ''}})
    .find({extras: {$in: 'laser light'}})

  console.log(cars)
}


/**
 * OPERADORES GET 
 * "$eq" => IGUAL (Equal)
 * "$ne" => DIFERENTE (No Equal)
 * "$gt" => MAYOR A (Greater Than)
 * "$gte" => MAYOR A O IGUAL A (Greater Than or Equal)
 * "$lt" => MENOR A (Less Than)
 * "$lte" => MENOR A O IGUAL A (Less Than or Equal)
 */
async function getFilterPriceCars() {
  const cars = await Car
    // .find({price: {$gt: 2000}})
    .find({price: {$gte: 2000, $lt: 2700}})

  console.log(cars)
}


 /**
  * GET
  */
async function getMoreFilterCar() {
  const cars = await Car
    .find({company: 'BMW', sold: false})
    .sort({price: 1}) // -1 para ordenar de mayor a menor o 1 para ordenar de menor a mayor
    .limit(2) // Retorna el número de documentos indicados en el parámetro
    .select({company: 1, model: 1, price: 1}) // Solo envia los parámetros indicados
      
  console.log(cars)
}

async function getCompanyAndSoldFilterCars() {
  const cars = await Car.find({
    company: 'BMW',
    sold: false
  })
  console.log(cars);
}

async function getAllCars() {
  const cars = await Car.find();
  console.log(cars);
}


/**
 * CREATE
 */
async function createCar() {
  // 'car' es una instancia de la clase 'Car', lo cual nos permite utilizar todas las funciones de mongoose
  const car = new Car({
    company: 'BMW',
    model: 'A1',
    price: 1900,
    year: 2018,
    sold: true,
    extras: ['laser light', '4x4']
  })

  const result = await car.save();
  console.log(result)
}
